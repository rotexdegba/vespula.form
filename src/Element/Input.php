<?php
namespace Vespula\Form\Element;

/**
 * Abstract class for input elements
 *
 * @author Jon Elofson <jon.elofson@gmail.com>
 */
abstract class Input extends Element
{
    /**
     * The input type (required)
     * @var string
     */
    protected $type;

    /**
     * Constructor. Ensures a type is set
     *
     * @throws \Exception
     */
    public function __construct()
    {
        parent::__construct();
        if (! $this->type) {
            throw new \Exception('Missing type');
        }
        $this->attributes->set('type', $this->type);
    }
    
    /**
     * Get the input type
     * 
     * @return string The input type
     */
    public function getType()
    {
        return $this->type;
    }
}
