<?php
namespace Vespula\Form\Element;

use Vespula\Form\Attributes;

/**
 * An abstract class from which all elements extend
 *
 * @author Jon Elofson <jon.elofson@gmail.com>
 */
abstract class Element implements ElementInterface
{
    /**
     * Element attributes
     * @var \Vespula\Form|Attributes
     */
    protected $attributes;

    /**
     * Set a line feed for the string output
     * @var [type]
     */
    protected $lf;

    /**
     * Valid attributes with shortcut methods via __call()
     * @var array
     */
    protected $validAttribs = [
        'id',
        'name',
        'value',
        'class',
        'maxlength',
        'size',
        'checked',
        'style',
        'required',
        'readonly',
        'disabled',
        'placeholder'
    ];

    public static $autoLf = false;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->attributes = new Attributes();
    }
    
    /**
     * Magic method to set attributes
     *
     * @param  string $method  The attribute name
     * @param  array $attribs Additional parameters
     * @return \Vespula\Form\Element\Element
     */
    public function __call($method, $attribs = null)
    {
        if (!in_array($method, $this->validAttribs)) {
            throw new \InvalidArgumentException('The attribute ' . $method . ' is not valid for this element');
        }

        $value = null;

        if (isset($attribs[0])) {
            $value = $attribs[0];
        }

        if (is_array($value)) {
            $value = implode(' ', $value);
        }

        $nullable = [
            'readonly',
            'disabled',
            'checked',
            'selected',
            'required'
        ];

        if (is_null($value) && ! in_array($method, $nullable)) {
            throw new \InvalidArgumentException('Attribute value can not be null');
        }

        $this->attributes->set($method, $value);
        return $this;
    }

    /**
     * Get The attributes object
     * @return array \Vespula\Form\Attributes
     */
    public function getAttributes()
    {
        return $this->attributes;
    }

    public function getAttribute($key)
    {
        return $this->attributes->get($key);
    }

    /**
     * Add an attribute to the attribute store
     * @param  string $key Attribute name
     * @param  mixed $val Attribute value
     * @return \Vespula\Form\Element\Element
     */
    public function attribute($key, $val = null)
    {
        $this->attributes->set($key, $val);
        return $this;
    }

    /**
     * Set a data- attribute
     * @param  string $key The attribute without 'data-'
     * @param  string $val The data attribute value
     * @return \Vespula\Form\Element\Element
     */
    public function data($key, $val = null)
    {
        $dataKey = 'data-' . $key;
        $this->attributes->set($dataKey, $val);
        return $this;
    }

    /**
     * Create an aria- attribute
     * @param  string $key The attribute without 'aria-'
     * @param  string $val The attribute value
     * @return \Vespula\Form\Element\Element
     */
    public function aria($key, $val = null)
    {
        $ariaKey = 'aria-' . $key;
        $this->attributes->set($ariaKey, $val);
        return $this;
    }

    /**
     * Add a class to the existing classes attribute
     * @param string $class The class name
     * @return \Vespula\Form\Element\Element
     */
    public function addClass($class)
    {
        $classes = [];
        if ($this->attributes->has('class')) {
            $classes = explode(' ', $this->attributes->get('class'));
        }

        $classes[] = $class;
        $classes = array_unique($classes);
        $this->attributes->set('class', implode(' ', $classes));
        return $this;
    }

    public function valueRaw($value)
    {
        $this->attributes->set('value', $value);
        return $this;
    }

    public function value($value)
    {
        $valueEscaped = htmlspecialchars($value, ENT_COMPAT | ENT_HTML401, 'UTF-8');
        $this->attributes->set('value', $valueEscaped);
        return $this;
    }

    /**
     * Allows you to set both the `id` and `name` attribute on a form element in one call.
     *
     * @param string $name The value to use for name and id
     * @return $this
     */
    public function idName($name)
    {
        $this->attributes->set('id', $name);
        $this->attributes->set('name', $name);
        return $this;
    }


    /**
     * Set the lf property
     * @param  mixed $lf The line feed character
     * @return \Vespula\Form\Element\Element
     */
    public function lf($lf = PHP_EOL)
    {
        $this->lf = $lf;
        return $this;
    }
    
    /**
     * Turn autoLf on
     */
    public static function autoLfOn()
    {
        self::$autoLf = true;
    }

    /**
     * Turn autoLf off
     */
    public static function autoLfOff()
    {
        self::$autoLf = false;
    }
    
    /**
     * Check if the element is a label. Nice for looping through all elements
     * 
     * @return bool True if it is a label object
     */
    public function isLabel()
    {
        return $this instanceof Label;
    }
    
    /**
     * Check if the element is a hidden element. Nice for looping through all elements
     * 
     * @return bool True if it is a hidden element
     */
    public function isHidden()
    {
        return $this instanceof Hidden;
    }


}
