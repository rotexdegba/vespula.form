<?php
namespace Vespula\Form\Element;

/**
 * Create a radio input element
 *
 * @author Jon Elofson <jon.elofson@gmail.com>
 */
class Radio extends Input
{
    /**
     * Input type (radio)
     * @var string
     */
    protected $type = 'radio';

    /**
     * The checked value. Can be true or you can set to the value of the radio
     * @var boolean
     */
    protected $checked = false;

    /**
     * The label for the radio input
     * @var string
     */
    protected $label;
    
    /**
     * The value that should be submitted if this element is unchecked
     * @var string 
     */
    protected $unchecked_value = null;

    /**
     * Set the radio to be checked. You can set this to true to force checked,
     * or you can pass a value. If the value matches the radio's value attribute,
     * then the radio will be checked.
     *
     * @param  boolean|string $checked
     * @return \Vespula\Form\Element\Radio
     */
    public function checked($checked = true)
    {
        $this->checked = $checked;
        return $this;
    }
    
    /**
     * The value that should be submitted if this element is unchecked
     * 
     * @param string $val
     * @return \Vespula\Form\Element\Radio
     */
    public function uncheckedValue($val) {
        
        $this->unchecked_value = $val;
        return $this;
    }

    /**
     * Set the radio label
     * @param  string $label
     * @return \Vespula\Form\Element\Radio
     */
    public function label($label)
    {
        $this->label = $label;
        return $this;
    }

    /**
     * Output the radio element as an html tag
     * @return string
     */
    public function __toString(): string
    {
        if (self::$autoLf) {
            $this->lf();
        }
        $value = $this->attributes->has('value') ? $this->attributes->get('value') : null;
        $checked = $this->checked === $value ? true : false;
        $this->attributes->set('checked', $checked);
        if (is_bool($this->checked)) {
            $this->attributes->set('checked', $this->checked);
        }

        $id = $this->attributes->has('id') ? $this->attributes->get('id') : null;

        $input = '<input' . $this->attributes . ' />';
        
        if ( 
            $this->attributes->has('name')  
            && ! is_null($this->unchecked_value) 
            && is_scalar($this->unchecked_value) 
        ) {    
            // add hidden field containing value to be posted to the server if this element is unchecked
            $name = $this->attributes->get('name');
            $hidden_field = "<input name=\"{$name}\" value=\"{$this->unchecked_value}\" type=\"hidden\">";
            $input = $hidden_field . ' ' . $this->lf .$input;
        }

        if ($this->label) {
            $for = $id ? ' for="' . $id . '"' : '';
            $input .= '<label' . $for . '>' . $this->label . '</label>';
        }

        return  $input . $this->lf;

    }
}
