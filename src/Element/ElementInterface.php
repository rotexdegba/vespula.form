<?php
declare(strict_types=1);

namespace Vespula\Form\Element;

interface ElementInterface
{
    public function __toString(): string;
}