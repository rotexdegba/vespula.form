<?php
namespace Vespula\Form\Element;


/**
 * Creates a checkbox input
 *
 * @author Jon Elofson <jon.elofson@gmail.com>
 */
class Checkbox extends Radio
{
    /**
     * The input type (checkbox)
     * @var string
     */
    protected $type = 'checkbox';
}
