<?php
namespace Vespula\Form;
use Vespula\Form\Element\Textarea;
use PHPUnit\Framework\TestCase;

class TextareaTest extends TestCase
{

    public function testGeneric()
    {
        $textarea = new Textarea();
        $expected = '<textarea></textarea>';
        $actual = $textarea->__toString();

        $this->assertEquals($expected, $actual);
    }

    public function testValue()
    {
        $textarea = new Textarea();
        $textarea->value('FooBar');
        $expected = '<textarea>FooBar</textarea>';
        $actual = $textarea->__toString();

        $this->assertEquals($expected, $actual);
    }

    public function testCols()
    {
        $textarea = new Textarea();
        $textarea->cols(3);
        $expected = '<textarea cols="3"></textarea>';
        $actual = $textarea->__toString();

        $this->assertEquals($expected, $actual);
    }

    public function testRows()
    {
        $textarea = new Textarea();
        $textarea->rows(3);
        $expected = '<textarea rows="3"></textarea>';
        $actual = $textarea->__toString();

        $this->assertEquals($expected, $actual);
    }

    public function testLf()
    {
        $textarea = new Textarea();
        $textarea->lf();
        $expected = '<textarea></textarea>' . PHP_EOL;
        $actual = $textarea->__toString();

        $this->assertEquals($expected, $actual);
    }

    // Any call to __toString() should always return the same thing, if nothing  
    // has been changed. 
    public function testToString() 
    { 
        $textarea = new Textarea(); 
        $textarea->rows(3); 
        $textarea->value('Foobar'); 
        $expected = '<textarea rows="3">Foobar</textarea>'; 
        $first = $textarea->__toString(); 
        $actual = $textarea->__toString(); 
 
        $this->assertEquals($expected, $actual); 
    } 
}
