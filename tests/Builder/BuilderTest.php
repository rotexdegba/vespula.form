<?php
namespace Vespula\Form\Builder;

use Vespula\Form\Form;
use Vespula\Form\Builder\Builder;
use PHPUnit\Framework\TestCase;
use Doctrine\DBAL\DriverManager;

class BuilderTest extends TestCase
{
    protected $form;
    protected $builder;

    public function setUp(): void
    {

        $params = [
            'driver'=>'pdo_sqlite',
            'memory'=>true
        ];
        $conn = DriverManager::getConnection($params);
        //$pdo = new \PDO('sqlite::memory:');
        $this->createTables($conn->getNativeConnection());

        

        $this->form = new Form();
        $this->builder = new Builder($conn);

    }
    public function tearDown(): void
    {
        $this->form->reset();
    }

    protected function createTables($pdo)
    {
        $sql = 'CREATE TABLE "foobar" (
            "id" INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL ,
            "name" VARCHAR(50) NOT NULL ,
            "first_name" VARCHAR(50) NOT NULL ,
            "age" INTEGER(3) NOT NULL ,
            "published" CHAR(1) NOT NULL,
            "status" CHAR(1) NOT NULL DEFAULT S,
            "photo" BLOB,
            "created" DATETIME NOT NULL ,
            "profile" TEXT)';

        $result = $pdo->query($sql);
        
    }

    public function testBuild()
    {
        $form = $this->builder->build($this->form, 'foobar');
        $expected_id = '<input type="hidden" id="id" name="id" value="" />';
        $expected_name = '<input type="text" id="name" name="name" value="" maxlength="50" required />';
        $expected_age = '<input type="text" id="age" name="age" value="" maxlength="3" required />';
        $expected_published = '<input type="text" id="published" name="published" value="" maxlength="1" required />';
        $expected_status = '<input type="text" id="status" name="status" value="S" maxlength="1" required />';
        $expected_photo = '<input type="text" id="photo" name="photo" value="" />';
        $expected_created = '<input type="text" id="created" name="created" value="" required />';
        $expected_profile = '<textarea cols="10" rows="5" id="profile" name="profile"></textarea>';

        $this->assertEquals($expected_id, $form->getElement('id')->__toString());
        $this->assertEquals($expected_name, $form->getElement('name')->__toString());
        $this->assertEquals($expected_age, $form->getElement('age')->__toString());
        $this->assertEquals($expected_published, $form->getElement('published')->__toString());
        $this->assertEquals($expected_status, $form->getElement('status')->__toString());
        $this->assertEquals($expected_photo, $form->getElement('photo')->__toString());
        $this->assertEquals($expected_created, $form->getElement('created')->__toString());
        $this->assertEquals($expected_profile, $form->getElement('profile')->__toString());
    }

    public function testMapCheckbox()
    {
        $map = [
            'string'=>'checkbox'
        ];
        $this->builder->setMap($map);
        $form = $this->builder->build($this->form, 'foobar');

        $expected_published = '<input type="checkbox" id="published" name="published" value="" />';
        $expected_status = '<input type="checkbox" id="status" name="status" value="S" />';
        $this->assertEquals($expected_published, $form->getElement('published')->__toString());
        $this->assertEquals($expected_status, $form->getElement('status')->__toString());
    }
    public function testMapRadio()
    {
        $map = [
            'string'=>'radio'
        ];
        $this->builder->setMap($map);
        $form = $this->builder->build($this->form, 'foobar', ['status'=>'S']);

        $expected_published = '<input type="radio" id="published" name="published" value="" />';
        $expected_status = '<input type="radio" id="status" name="status" value="S" checked />';
        $this->assertEquals($expected_published, $form->getElement('published')->__toString());
        $this->assertEquals($expected_status, $form->getElement('status')->__toString());
    }

    public function testColumnOptionsSimple()
    {
        $this->builder->setColumnOptions([
            'photo'=>[
                'type'=>'textarea',
                'raw'=>true
            ]
        ]);

        $form = $this->builder->build($this->form, 'foobar', ['photo'=>'<p>Here is some raw data</p>']);
        $expected = '<textarea cols="10" rows="5" id="photo" name="photo"><p>Here is some raw data</p></textarea>';
        $this->assertEquals($expected, $form->getElement('photo')->__toString());
    }

    public function testColumnOptionsMerge()
    {
        $this->builder->setColumnOptions([
            'status'=>[
                'type'=>'textarea',
                'raw'=>true
            ]
        ]);

        $this->builder->setColumnOptions([
            'status'=>[
                'type'=>'checkbox'
            ]
        ]);

        $form = $this->builder->build($this->form, 'foobar');
        $expected = '<input type="checkbox" id="status" name="status" value="S" />';
        $this->assertEquals($expected, $form->getElement('status')->__toString());
    }

    public function testColumnOptionsCallback()
    {
        $this->builder->setColumnOptions([
            'photo'=>[
                'type'=>'textarea',
                'callback'=>function ($element) {
                    $element->value('Hello World')->class('foo')->required();
                }
            ]
        ]);

        $form = $this->builder->build($this->form, 'foobar');
        $expected = '<textarea cols="10" rows="5" id="photo" name="photo" class="foo" required>Hello World</textarea>';
        $this->assertEquals($expected, $form->getElement('photo')->__toString());
    }

    public function testLabel()
    {

        $form = $this->builder->build($this->form, 'foobar');
        $expected = '<label for="name">Name</label>';
        $expected_words = '<label for="first_name">First Name</label>';
        $this->assertEquals($expected, $form->getElement('label_name')->__toString());
        $this->assertEquals($expected_words, $form->getElement('label_first_name')->__toString());
    }

    public function testLabelDefaultClass()
    {
        $this->builder->setDefaultClasses([
            'label'=>'label-class'
        ]);
        $form = $this->builder->build($this->form, 'foobar');
        $expected = '<label for="name" class="label-class">Name</label>';
        $this->assertEquals($expected, $form->getElement('label_name')->__toString());
    }

    public function testPasswordValue()
    {

        $form = $this->builder->build($this->form, 'foobar', ['name'=>'foobar']);
        $expected = '<input type="text" id="name" name="name" value="foobar" maxlength="50" required />';
        $this->assertEquals($expected, $form->getElement('name')->__toString());

        $this->builder->setColumnOptions([
            'name'=>[
                'type'=>'password'
            ]
        ]);
        $form = $this->builder->build($this->form, 'foobar', ['name'=>'mypassword']);
        $expected_novalue = '<input type="password" id="name" name="name" value="" maxlength="50" required />';
        $this->assertEquals($expected_novalue, $form->getElement('name')->__toString());
    }

    public function testDataArray()
    {
        $data = include 'data/foobar.php';
        $form = $this->builder->build($this->form, 'foobar', $data);

        $expected_id = '<input type="hidden" id="id" name="id" value="2" />';
        $expected_name = '<input type="text" id="name" name="name" value="Joe" maxlength="50" required />';
        $expected_age = '<input type="text" id="age" name="age" value="26" maxlength="3" required />';
        $expected_published = '<input type="text" id="published" name="published" value="1" maxlength="1" required />';
        $expected_status = '<input type="text" id="status" name="status" value="M" maxlength="1" required />';
        $expected_photo = '<input type="text" id="photo" name="photo" value="" />';
        $expected_created = '<input type="text" id="created" name="created" value="2017-08-21 16:00:00" required />';
        $expected_profile = '<textarea cols="10" rows="5" id="profile" name="profile">Lorem ipsum dolar sit amet.</textarea>';

        $this->assertEquals($expected_id, $form->getElement('id')->__toString());
        $this->assertEquals($expected_name, $form->getElement('name')->__toString());
        $this->assertEquals($expected_age, $form->getElement('age')->__toString());
        $this->assertEquals($expected_published, $form->getElement('published')->__toString());
        $this->assertEquals($expected_status, $form->getElement('status')->__toString());
        $this->assertEquals($expected_photo, $form->getElement('photo')->__toString());
        $this->assertEquals($expected_created, $form->getElement('created')->__toString());
        $this->assertEquals($expected_profile, $form->getElement('profile')->__toString());
    }

    public function testDataArrayAccess()
    {
        include 'data/obj.php';
        $data = new Obj();
        $form = $this->builder->build($this->form, 'foobar', $data);

        $expected_id = '<input type="hidden" id="id" name="id" value="2" />';
        $expected_name = '<input type="text" id="name" name="name" value="Joe" maxlength="50" required />';
        $expected_age = '<input type="text" id="age" name="age" value="26" maxlength="3" required />';
        $expected_published = '<input type="text" id="published" name="published" value="1" maxlength="1" required />';
        $expected_status = '<input type="text" id="status" name="status" value="M" maxlength="1" required />';
        $expected_photo = '<input type="text" id="photo" name="photo" value="" />';
        $expected_created = '<input type="text" id="created" name="created" value="2017-08-21 16:00:00" required />';
        $expected_profile = '<textarea cols="10" rows="5" id="profile" name="profile">Lorem ipsum dolar sit amet.</textarea>';

        $this->assertEquals($expected_id, $form->getElement('id')->__toString());
        $this->assertEquals($expected_name, $form->getElement('name')->__toString());
        $this->assertEquals($expected_age, $form->getElement('age')->__toString());
        $this->assertEquals($expected_published, $form->getElement('published')->__toString());
        $this->assertEquals($expected_status, $form->getElement('status')->__toString());
        $this->assertEquals($expected_photo, $form->getElement('photo')->__toString());
        $this->assertEquals($expected_created, $form->getElement('created')->__toString());
        $this->assertEquals($expected_profile, $form->getElement('profile')->__toString());
    }
}
