<?php
namespace Vespula\Form;

use Vespula\Form\Builder\Builder;
use PHPUnit\Framework\TestCase;
use Doctrine\DBAL\DriverManager;

class FormTest extends TestCase
{
    public function testEnd()
    {
        $form = new Form('get');

        $end = $form->end();

        $expected = '</form>' . PHP_EOL;

        $this->assertEquals($expected, $end);
    }

    public function testId()
    {
        $form = new Form('get');
        $form->id('my-form');

        $start = $form->begin();

        $expected = '<form method="get" id="my-form">' . PHP_EOL;

        $this->assertEquals($expected, $start);

    }

    public function testMethod()
    {
        $form = new Form('get');
        $form->method('post');

        $start = $form->begin();

        $expected = '<form method="post">' . PHP_EOL;

        $this->assertEquals($expected, $start);
    }

    public function testPost()
    {
        $form = new Form('post');
        $start = $form->begin();

        $expected = '<form method="post">' . PHP_EOL;

        $this->assertEquals($expected, $start);

    }

    public function testAction()
    {
        $form = new Form();
        $form->action('/my/path');
        $start = $form->begin();

        $expected = '<form method="post" action="/my/path">' . PHP_EOL;

        $this->assertEquals($expected, $start);

    }

    public function testName()
    {
        $form = new Form();
        $form->name('my-form');
        $start = $form->begin();

        $expected = '<form method="post" name="my-form">' . PHP_EOL;

        $this->assertEquals($expected, $start);

    }

    public function testClass()
    {
        $form = new Form();
        $form->class('foo');
        $start = $form->begin();

        $expected = '<form method="post" class="foo">' . PHP_EOL;

        $this->assertEquals($expected, $start);

    }

    public function testMultipart()
    {
        $form = new Form();
        $form->multipart();
        $start = $form->begin();

        $expected = '<form method="post" enctype="multipart/form-data">' . PHP_EOL;

        $this->assertEquals($expected, $start);

    }

    public function testAttribute()
    {
        $form = new Form();
        $form->attribute('class', 'foo');
        $start = $form->begin();

        $expected = '<form method="post" class="foo">' . PHP_EOL;

        $this->assertEquals($expected, $start);

    }

    public function testAddGetElement()
    {
        $form = new Form();

        $text = $form->text();
        $form->addElement('mytext', $text);

        $input = $form->getElement('mytext');
        $actual = $input->__toString();

        $expected = '<input type="text" />';

        $this->assertEquals($expected, $actual);

    }

    public function testReorderElements()
    {
        $form = new Form();

        $form->addElement('d', $form->password());
        $form->addElement('c', $form->radio());
        $form->addElement('b', $form->select());
        $form->addElement('a', $form->submit());
        $form->addElement('e', $form->text());
        $form->addElement('f', $form->textarea());
        
        // reorder first four, last two will stay as is
        $form->reorderElements(['a', 'b', 'c', 'd']);
        
        $expected_order_of_ids = ['a', 'b', 'c', 'd', 'e', 'f'];
        
        foreach( $form->getElements() as $id => $element ) {
            
            $this->assertEquals(array_shift($expected_order_of_ids), $id);
        }
        
        // reorder ids in reverse alpha order
        $form->reorderElements(['f', 'e', 'd', 'c', 'b', 'a']);
        
        $expected_order_of_ids = ['f', 'e', 'd', 'c', 'b', 'a'];
        
        foreach( $form->getElements() as $id => $element ) {
            
            $this->assertEquals(array_shift($expected_order_of_ids), $id);
        }

        // make sure the elements are still of the right types after 
        // re-ordering
        $this->assertTrue(
            ($form->getElement('a') instanceof \Vespula\Form\Element\Button)
            && $form->getElement('a')->getAttribute('type') === 'submit'
        );
        $this->assertTrue(($form->getElement('b') instanceof \Vespula\Form\Element\Select));
        $this->assertTrue(($form->getElement('c') instanceof \Vespula\Form\Element\Radio));
        $this->assertTrue(($form->getElement('d') instanceof \Vespula\Form\Element\Password));
        $this->assertTrue(($form->getElement('e') instanceof \Vespula\Form\Element\Text));
        $this->assertTrue(($form->getElement('f') instanceof \Vespula\Form\Element\Textarea));
    }

    public function testGetElements()
    {
        $form = new Form();

        $text = $form->text();
        $form->addElement('mytext', $text);
        $form->addElement('myothertext', $text);

        $elements = $form->getElements();


        $expected = [
            'mytext'=>$text,
            'myothertext'=>$text
        ];

        $this->assertEquals($expected, $elements);
    }

    public function testGetElementsSpecific()
    {
        $form = new Form();

        $text = $form->text();
        $form->addElement('mytext', $text);
        $form->addElement('myothertext', $text);

        $elements = $form->getElements(['mytext', 'myothertext']);


        $expected = [
            'mytext'=>$text,
            'myothertext'=>$text
        ];

        $this->assertEquals($expected, $elements);
    }

    public function testAddElementLabel()
    {
        $form = new Form();

        $text = $form->text();
        $label = $form->label('foo')->wrap($text);
        $form->addElement('label', $label);

        $input = $form->getElement('label');
        $actual = $input->__toString();

        $expected = '<label>foo' . PHP_EOL .
                    '    <input type="text" />' . PHP_EOL .
                    '</label>';

        $this->assertEquals($expected, $actual);

    }

    public function testLabel()
    {
        $form = new Form();
        $label = $form->label('My Label');
        $actual = $label->__toString();
        $expected = '<label>My Label</label>';
        $this->assertEquals($expected, $actual);

    }

    public function testText()
    {
        $form = new Form();
        $text = $form->text();
        $actual = $text->__toString();
        $expected = '<input type="text" />';
        $this->assertEquals($expected, $actual);
    }

    public function testPassword()
    {
        $form = new Form();
        $password = $form->password();
        $actual = $password->__toString();
        $expected = '<input type="password" />';
        $this->assertEquals($expected, $actual);
    }

    public function testTextarea()
    {
        $form = new Form();
        $textarea = $form->textarea()->value('foo');
        $actual = $textarea->__toString();
        $expected = '<textarea>foo</textarea>';
        $this->assertEquals($expected, $actual);
    }

    public function testSelect()
    {
        $form = new Form();
        $options = [
            'cat',
            'dog'
        ];
        $textarea = $form->select()->options($options);
        $actual = $textarea->__toString();
        $expected = '<select>' . "\n" .
                    '    <option value="0">cat</option>' . "\n" .
                    '    <option value="1">dog</option>' . "\n" .
                    '</select>';
        $this->assertEquals($expected, $actual);
    }

    public function testRadio()
    {
        $form = new Form();
        $radio = $form->radio()->label('foo')->id('bar');
        $actual = $radio->__toString();
        $expected = '<input type="radio" id="bar" /><label for="bar">foo</label>';
        $this->assertEquals($expected, $actual);
    }

    public function testCheckbox()
    {
        $form = new Form();
        $radio = $form->checkbox()->label('foo')->id('bar');
        $actual = $radio->__toString();
        $expected = '<input type="checkbox" id="bar" /><label for="bar">foo</label>';
        $this->assertEquals($expected, $actual);
    }

    public function testButton()
    {
        $form = new Form();
        $radio = $form->button()->text('My Button');
        $actual = $radio->__toString();
        $expected = '<button type="button">My Button</button>';
        $this->assertEquals($expected, $actual);
    }

    public function testSubmit()
    {
        $form = new Form();
        $radio = $form->submit()->text('My Submit');
        $actual = $radio->__toString();
        $expected = '<button type="submit">My Submit</button>';
        $this->assertEquals($expected, $actual);
    }

    public function testReset()
    {
        $form = new Form();
        $form->reset();
        $expectedBegin = '<form>' . PHP_EOL;
        $this->assertEquals($expectedBegin, $form->begin());
        $this->assertFalse(\Vespula\Form\Element\Element::$autoLf);
    }

    /**
     * @expectedException Exception
     */
    public function testResetException()
    {
        $this->expectException(\Exception::class);
        $form = new Form();
        $form->id('bar')->name('my-form');
        $form->addElement('foo', $form->text());

        $form->reset();

        $form->getElement('foo');

    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testClassException()
    {
        $this->expectException(\InvalidArgumentException::class);
        $form = new Form();
        $form->donkey();
    }

    public function testHidden()
    {
        $form = new Form();
        $hidden = $form->hidden();
        $actual = $hidden->__toString();
        $expected = '<input type="hidden" />';
        $this->assertEquals($expected, $actual);
    }

    public function testAutoLf()
    {
        $form = new Form();
        $form->autoLf();
        $element = $form->text();
        $actual = $element->__toString();
        $expected = '<input type="text" />' . PHP_EOL;

        // set back to false for other tests to work.
        \Vespula\Form\Element\Element::$autoLf = false;

        $this->assertEquals($expected, $actual);
    }

    public function testAutoLfOff()
    {
        $form = new Form();
        $form->autoLf();
        $form->autoLfOff();
        $element = $form->text();
        $actual = $element->__toString();
        $expected = '<input type="text" />';

        $this->assertEquals($expected, $actual);
    }

    public function testSetGetBuilder()
    {
        $params = [
            'driver'=>'pdo_sqlite',
            'memory'=>true
        ];
        $conn = DriverManager::getConnection($params);
        
        //$pdo = new \PDO('sqlite::memory:');
        $builder = new Builder($conn);

        $form = new Form();
        $form->setBuilder($builder);

        $this->assertEquals($builder, $form->getBuilder());
    }

    public function testBuild()
    {
        $params = [
            'driver'=>'pdo_sqlite',
            'memory'=>true
        ];
        $conn = DriverManager::getConnection($params);
        $pdo = $conn->getNativeConnection();

        $sql = 'CREATE TABLE "foobar" (
            "id" INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL ,
            "name" VARCHAR(50) NOT NULL ,
            "first_name" VARCHAR(50) NOT NULL ,
            "age" INTEGER(3) NOT NULL ,
            "published" CHAR(1) NOT NULL,
            "status" CHAR(1) NOT NULL DEFAULT S,
            "photo" BLOB,
            "created" DATETIME NOT NULL ,
            "profile" TEXT)';

        $result = $pdo->query($sql);

        $builder = new Builder($conn);

        $form = new Form();
        $form->setBuilder($builder);

        $form->build('foobar');

        $expected = '<input type="hidden" id="id" name="id" value="" />';

        $this->assertEquals($expected, $form->getElement('id'));
    }
}
