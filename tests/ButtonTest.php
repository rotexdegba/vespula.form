<?php
namespace Vespula\Form;
use Vespula\Form\Element\Button;
use PHPUnit\Framework\TestCase;

class ButtonTest extends TestCase
{
    public function testTypeButton()
    {
        $button = new Button();
        $button->text('My Button');
        $expected  = '<button type="button">My Button</button>';

        $actual = $button->__toString();
        $this->assertEquals($expected, $actual);
    }

    public function testTypeSubmit()
    {
        $button = new Button();
        $button->text('My Button')->type('submit');
        $expected  = '<button type="submit">My Button</button>';

        $actual = $button->__toString();
        $this->assertEquals($expected, $actual);
    }

    public function testLf()
    {
        $button = new Button();
        $button->text('My Button')->type('submit')->lf();
        $expected  = '<button type="submit">My Button</button>' . PHP_EOL;

        $actual = $button->__toString();
        $this->assertEquals($expected, $actual);
    }
}
