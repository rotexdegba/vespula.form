README
========

This package is used to simplify the generation of HTML form inputs and the form
tag itself. It's meant to be very simple without extra features you might find in
other packages. For example, you wont find validation here, or saving state. Just
dead-simple form elements.

The package is fully tested, but if you find any missing coverage, just let me know.

## Form Builder (new as of 0.4.0)

Be sure to look at the documentation on the new form builder. The builder can 
create elements for you based on a database table. This makes it much faster to 
create the initial elements. Also, you can pass an array or an object that employs 
ArrayAccess to the builder to set the initial values of the form elements.

## Documentation

Documentation is available at
https://vespula.bitbucket.io/form/
